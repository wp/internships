package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.service.*;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("/student-internships")
public class StudentController {
    private final InternshipService internshipService;
    private final InternshipWeekService internshipWeekService;
    private final StudentService studentService;
    private final CompanyService companyService;

    @GetMapping
    public String showList(@RequestParam(required = false) InternshipStatus status,
                           @RequestParam(required = false) String professorName,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "10") Integer results,
                           Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentIndex = authentication.getName();
        Student student = studentService.getStudentByIndex(studentIndex);

        Page<Internship> internshipsPage = internshipService.listInternships(pageNum, results, status, studentIndex, null, professorName);

        model.addAttribute("statusSelected", status);
        model.addAttribute("statuses", InternshipStatus.values());
        model.addAttribute("professorName", professorName);
        model.addAttribute("student", student);
        model.addAttribute("page", internshipsPage);

        return "student/listInternships";
    }

    @GetMapping("/{internshipId}")
    public String showDetails(@PathVariable Long internshipId,
                              Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentIndex = authentication.getName();
        Student student = this.studentService.getStudentByIndex(studentIndex);

        Internship internship = this.internshipService.findById(internshipId);
        if (internship.getStudent() != student) {
            model.addAttribute("status", 403);
            model.addAttribute("error", "Forbidden");
            return "error";
        }
        Professor professor = internship.getCoordinator();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        int totalWeeks = journal.size();

        model.addAttribute("professor", professor);
        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("journal", journal);
        model.addAttribute("totalWeeks", totalWeeks);

        return "student/journalDetails";
    }

    @PostMapping("/{internshipId}/internship-weeks/{weekId}/save")
    public String saveWeek(@PathVariable Long internshipId,
                           @PathVariable Long weekId,
                           @RequestParam LocalDate startDate,
                           @RequestParam LocalDate endDate,
                           @RequestParam int workingHours,
                           @RequestParam(required = false) String description,
                           Model model,
                           RedirectAttributes redirectAttributes) {
        if (startDate.isAfter(endDate)) {
            redirectAttributes.addFlashAttribute("error", "Почетниот датум мора да биде пред крајниот датум.");
            return "redirect:/student-internships/edit/" + internshipId;
        }
        internshipWeekService.update(weekId, startDate, endDate, internshipId, description, workingHours);
        return "redirect:/student-internships/edit/" + internshipId;
    }

    @PostMapping("/{internshipId}/update-dates")
    public String saveWeek(@PathVariable Long internshipId,
                           @RequestParam LocalDate internshipStartDate,
                           @RequestParam LocalDate internshipEndDate,
                           RedirectAttributes redirectAttributes,
                           Model model) {
        if (internshipStartDate.isAfter(internshipEndDate)) {
            redirectAttributes.addFlashAttribute("error", "Почетниот датум мора да биде пред крајниот датум.");
            return "redirect:/student-internships/edit/" + internshipId;
        }
        internshipService.updateDates(internshipId, internshipStartDate, internshipEndDate);
        return "redirect:/student-internships/edit/" + internshipId;
    }


    @GetMapping("/add")
    public String showAdd(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentIndex = authentication.getName();
        Student student = studentService.getStudentByIndex(studentIndex);

        model.addAttribute("student", student);
        model.addAttribute("companies", companyService.getAllCompanies());

        return "student/createInternships";
    }

    @GetMapping("/{internshipId}/addWeek")
    public String showAddWeek(@PathVariable Long internshipId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentIndex = authentication.getName();
        Student student = studentService.getStudentByIndex(studentIndex);
        Internship internship = internshipService.findById(internshipId);
        if (internship.getStudent() != student) {
            model.addAttribute("status", 403);
            model.addAttribute("error", "Forbidden");
            return "error";
        }
        Boolean isAccepted = internship.getStatus() == InternshipStatus.ACCEPTED;

        model.addAttribute("student", student);
        model.addAttribute("isAccepted", isAccepted);

        return "student/createInternshipWeek";
    }


    @PostMapping("/create")
    public String createInternship(
            @RequestParam String companyId,
            @RequestParam String description,
            @RequestParam LocalDate startDate,
            @RequestParam LocalDate endDate,
            @RequestParam(required = false, defaultValue = "false") boolean foreignCompany,
            @RequestParam int weeklyHours,
            RedirectAttributes redirectAttributes,
            Model model) {
        try {
            if (startDate.isAfter(endDate)) {
                redirectAttributes.addFlashAttribute("error", "Почетниот датум мора да биде пред крајниот датум.");
                return "redirect:/student-internships/add";
            }
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String studentIndex = authentication.getName();
            this.internshipService.createInternshipEmail(studentIndex, companyId, description,
                    startDate, endDate, foreignCompany, weeklyHours);
            return "redirect:/student-internships";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "student/createInternships";
        }
    }

    @PostMapping("/{internshipId}/createInternshipWeek")
    public String createInternshipWeek(
            @PathVariable Long internshipId,
            @DateTimeFormat LocalDate startDate,
            @DateTimeFormat LocalDate endDate,
            @RequestParam String description,
            @RequestParam int workingHours,
            RedirectAttributes redirectAttributes,
            Model model) {
        try {
            if (startDate.isAfter(endDate)) {
                redirectAttributes.addFlashAttribute("error", "Почетниот датум мора да биде пред крајниот датум.");
                return "redirect:/student-internships/add";
            }
            this.internshipWeekService.create(startDate, endDate, internshipId, description, workingHours);
            return "redirect:/student-internships/{internshipId}";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "student/createInternshipWeek";
        }
    }


    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String studentIndex = authentication.getName();
        Student student = this.studentService.getStudentByIndex(studentIndex);

        Internship internship = this.internshipService.findById(id);
        if (internship.getStudent() != student) {
            model.addAttribute("status", 403);
            model.addAttribute("error", "Forbidden");
            return "error";
        }
        Professor professor = internship.getCoordinator();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        int totalWeeks = journal.size();
        model.addAttribute("totalWeeks", totalWeeks);

        model.addAttribute("professor", professor);
        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("journal", journal);

        return "student/journalEdit";
    }


    @PostMapping("/delete/{id}")
    public String deleteInternship(@PathVariable Long id) {
        Internship internship = internshipService.findById(id);
        if (internship.getStatus() != InternshipStatus.SUBMITTED)
            return "error";
        this.internshipService.delete(id);
        return "redirect:/student-internships";
    }

    @GetMapping("/{internshipId}/internship-weeks/{weekId}/delete")
    public String deleteInternshipWeek(@PathVariable Long internshipId,
                                       @PathVariable Long weekId,
                                       RedirectAttributes redirectAttributes) {
        Internship internship = internshipService.findById(internshipId);
        if (internship.getStatus() != InternshipStatus.ACCEPTED)
            return "error";
        try {
            internshipWeekService.delete(weekId);
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", "Грешка при бришење на неделата.");
        }
        return "redirect:/student-internships/edit/" + internshipId;
    }

    @GetMapping("/submit/{id}")
    public String submit(@PathVariable Long id) {
        this.internshipService.submitJournal(id);
        return "redirect:/student-internships";
    }

}

