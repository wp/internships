package mk.ukim.finki.wp.internships.service;

import mk.ukim.finki.wp.internships.model.internships.InternshipCoordinatorAssignmentsView;

import java.util.List;

public interface CoordinatorAssignmentsService {
    List<InternshipCoordinatorAssignmentsView> getCoordinatorAssignments();
}
