package mk.ukim.finki.wp.internships.service.impl;

import mk.ukim.finki.wp.internships.model.dtos.MailSendingStatus;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.service.EmailService;
import mk.ukim.finki.wp.internships.service.NotificationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final EmailService emailService;

    private final String baseUrl;

    public NotificationServiceImpl(EmailService emailService,
                                   @Value("${app.base-url}") String baseUrl) {
        this.emailService = emailService;
        this.baseUrl = baseUrl;
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> notifyCompanyInternshipCreated(Internship internship) {
        Map<String, Object> model = new HashMap<>();
        model.put("companyToken", internship.getCompanyToken());
        model.put("student", internship.getStudent().getName()+" "+internship.getStudent().getLastName());
        model.put("internship", internship);

        String acceptLink = String.format("%s/company-action/accept/%s", baseUrl, internship.getCompanyToken());
        String rejectLink = String.format("%s/company-action/reject/%s", baseUrl, internship.getCompanyToken());
        model.put("acceptLink", acceptLink);
        model.put("rejectLink", rejectLink);

        String subject = "Барање за студентска пракса";
        String template = "company-internship-action";

        List<CompletableFuture<MailSendingStatus>> mailStatuses = new ArrayList<>();
        mailStatuses.add(emailService.sendMail(new String[]{internship.getCompany().email}, subject, template, null, model, null));
        return mailStatuses;
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> notifyCompanyInternshipJournalSubmitted(Internship internship) {
        Map<String, Object> model = new HashMap<>();
        model.put("companyToken", internship.getCompanyToken());
        model.put("student", internship.getStudent().getName()+" "+internship.getStudent().getLastName());
        model.put("internship", internship);

        String validateLink = String.format("%s/company-action/valid/%s", baseUrl, internship.getCompanyToken());
        String invalidateLink = String.format("%s/company-action/invalid/%s", baseUrl, internship.getCompanyToken());
        String journalLink = String.format("%s/company-action/journal/%s", baseUrl, internship.getCompanyToken());
        model.put("validateLink", validateLink);
        model.put("invalidateLink", invalidateLink);
        model.put("journalLink", journalLink);

        String subject = "Завршен дневник за студентска пракса";
        String template = "company-internship-journal-action";

        List<CompletableFuture<MailSendingStatus>> mailStatuses = new ArrayList<>();
        mailStatuses.add(emailService.sendMail(new String[]{internship.getCompany().email}, subject, template, null, model, null));
        return mailStatuses;
    }
}
