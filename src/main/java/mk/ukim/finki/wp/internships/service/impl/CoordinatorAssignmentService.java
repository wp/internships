package mk.ukim.finki.wp.internships.service.impl;

import mk.ukim.finki.wp.internships.model.internships.InternshipCoordinatorAssignmentsView;
import mk.ukim.finki.wp.internships.repository.CoordinatorAssignmentsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CoordinatorAssignmentService {
    private final List<String> coordinators;
    private final CoordinatorAssignmentsRepository coordinatorAssignmentsRepository;

    public CoordinatorAssignmentService(
            @Value("${internships.coordinators}") String coordinatorList,
            CoordinatorAssignmentsRepository coordinatorAssignmentsRepository) {
        this.coordinators = Arrays.asList(coordinatorList.split(","));
        this.coordinatorAssignmentsRepository = coordinatorAssignmentsRepository;
    }

    public synchronized String getNextCoordinator() {
        if (coordinators.isEmpty()) {
            throw new IllegalStateException("No coordinators available.");
        }

        List<InternshipCoordinatorAssignmentsView> assignments = coordinatorAssignmentsRepository.findAll();

        Map<String, Integer> coordinatorAssignments = assignments.stream()
                .filter(assignment -> coordinators.contains(assignment.getProfessorId()))
                .collect(Collectors.toMap(
                        InternshipCoordinatorAssignmentsView::getProfessorId,
                        InternshipCoordinatorAssignmentsView::getAssignmentsCount));

        return coordinatorAssignments.entrySet().stream()
                .min(Comparator.comparingInt(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .orElseThrow(() -> new IllegalStateException("No coordinators available."));
    }
}
