package mk.ukim.finki.wp.internships.repository;

import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;

public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {
    Professor findByEmail(String email);

}
