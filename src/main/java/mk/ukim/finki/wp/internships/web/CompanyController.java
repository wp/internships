package mk.ukim.finki.wp.internships.web;

import mk.ukim.finki.wp.internships.model.Company;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.service.CompanyService;
import mk.ukim.finki.wp.internships.service.InternshipService;
import mk.ukim.finki.wp.internships.service.InternshipWeekService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@Controller
@RequestMapping("/company-internships")
public class CompanyController {
    private final InternshipService internshipService;
    private final CompanyService companyService;
    private final InternshipWeekService internshipWeekService;

    public CompanyController(InternshipService internshipService, CompanyService companyService, InternshipWeekService internshipWeekService) {
        this.internshipService = internshipService;
        this.companyService = companyService;
        this.internshipWeekService = internshipWeekService;
    }

    @GetMapping
    public String showList(@RequestParam(required = false) InternshipStatus status,
                           @RequestParam(required = false) String studentName,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "10") Integer results,
                           Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String companyId = authentication.getName();
        Company company = companyService.findById(companyId);

        Page<Internship> internshipsPage = internshipService.listInternships(pageNum, results, status, studentName, companyId, null);

        model.addAttribute("page", internshipsPage);
        model.addAttribute("company", company);
        model.addAttribute("statuses", InternshipStatus.values());
        model.addAttribute("statusSelected", status);
        model.addAttribute("studentName", studentName);

        return "company/listInternships";
    }

    @GetMapping("/{internshipId}")
    public String internshipDetails(@PathVariable Long internshipId,
                                    Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String companyId = authentication.getName();
        Internship internship = this.internshipService.findById(internshipId);
        Student student = internship.getStudent();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        boolean displayJournal = internship.getStatus() == InternshipStatus.JOURNAL_SUBMITTED ||
                internship.getStatus() == InternshipStatus.VALIDATED_BY_COMPANY ||
                internship.getStatus() == InternshipStatus.VALIDATED_BY_COORDINATOR ||
                internship.getStatus() == InternshipStatus.ARCHIVED;
        int totalWeeks = journal.size();
        model.addAttribute("totalWeeks", totalWeeks);

        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("displayJournal", displayJournal);
        model.addAttribute("journal", journal);
        model.addAttribute("companyId", companyId);

        return "company/journalDetails";
    }

    @PostMapping("/{internshipId}/accept")
    public String acceptInternship(@PathVariable Long internshipId) {
        internshipService.updateStatus(internshipId, InternshipStatus.ACCEPTED);
        return "redirect:/company-internships";
    }

    @PostMapping("/{internshipId}/reject")
    public String rejectInternship(@PathVariable Long internshipId) {
        internshipService.updateStatus(internshipId, InternshipStatus.REJECTED);
        return "redirect:/company-internships";
    }

    @PostMapping("/{internshipId}/valid")
    public String validateInternship(@PathVariable Long internshipId) {
        internshipService.updateStatus(internshipId, InternshipStatus.VALIDATED_BY_COMPANY);
        return "redirect:/company-internships";
    }

    @PostMapping("/{internshipId}/invalid")
    public String internshipNotValid(@PathVariable Long internshipId) {
        internshipService.updateStatus(internshipId, InternshipStatus.ACCEPTED);
        return "redirect:/company-internships";
    }

    @PostMapping("/{internshipId}/internship-weeks/{weekId}/addWeekComment")
    public String addWeekComment(@PathVariable Long internshipId,
                                 @PathVariable Long weekId,
                                 @RequestParam String companyComment) {
        if (companyComment.isEmpty()) {
            companyComment = null;
        }
        this.internshipWeekService.updateCompanyComment(weekId, companyComment);

        return "redirect:/company-internships/" + internshipId;
    }
}
