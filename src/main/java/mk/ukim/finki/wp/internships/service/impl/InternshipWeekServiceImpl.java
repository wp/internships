package mk.ukim.finki.wp.internships.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.exception.EntityNotFoundException;
import mk.ukim.finki.wp.internships.exception.InternshipNotFoundException;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.repository.internships.InternshipRepository;
import mk.ukim.finki.wp.internships.repository.internships.InternshipWeekRepository;
import mk.ukim.finki.wp.internships.service.InternshipWeekService;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class InternshipWeekServiceImpl implements InternshipWeekService {
    InternshipWeekRepository internshipWeekRepository;
    InternshipRepository internshipRepository;

    @Override
    public InternshipWeek findById(Long id) {
        return internshipWeekRepository.findById(id).orElseThrow();
    }

    @Override
    public InternshipWeek create(LocalDate startDate, LocalDate endDate, Long internshipId, String description,
                                 int workingHours) {
        Internship internship = internshipRepository.findById(internshipId)
                .orElseThrow(() -> new InternshipNotFoundException(internshipId));

        InternshipWeek internshipWeek = new InternshipWeek(startDate,endDate,internship,description, workingHours);

        return internshipWeekRepository.save(internshipWeek);
    }

    @Override
    public InternshipWeek updateDescription(Long id, String description) {
        InternshipWeek week = internshipWeekRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("InternshipWeek with id "+id+" not found"));
        week.setDescription(description);
        return week;
    }

    @Override
    public InternshipWeek update(Long id, LocalDate startDate, LocalDate endDate, Long internshipId, String description,
                                 int workingHours) {
        InternshipWeek week = internshipWeekRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("InternshipWeek with id "+id+" not found"));
        week.setStartDate(startDate);
        week.setEndDate(endDate);
        week.setDescription(description);
        week.setWorkingHours(workingHours);
        return internshipWeekRepository.save(week);
    }

    @Override
    public InternshipWeek delete(Long id) {
        InternshipWeek res = internshipWeekRepository.findById(id).orElseThrow();
        internshipWeekRepository.delete(res);
        return res;
    }

    @Override
    public InternshipWeek updateCoordinatorComment(Long id, String coordinatorComment) {
        InternshipWeek week = internshipWeekRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("InternshipWeek with id "+id+" not found"));
        week.setCoordinatorComment(coordinatorComment);
        return internshipWeekRepository.save(week);
    }

    @Override
    public InternshipWeek updateCompanyComment(Long id, String companyComment) {
        InternshipWeek week = internshipWeekRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("InternshipWeek with id "+id+" not found"));
        week.setCompanyComment(companyComment);
        return internshipWeekRepository.save(week);
    }
}
