package mk.ukim.finki.wp.internships.model.internships;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.wp.internships.model.Company;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Internship {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_index")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor coordinator;

    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @Enumerated(EnumType.STRING)
    private InternshipStatus status;

    @OneToMany(mappedBy = "internship",cascade = CascadeType.ALL)
    private List<InternshipWeek> journal;

    private String description;

    private LocalDate startDate;

    private LocalDate endDate;

    private Integer weeklyHours;

    private boolean foreignCompany;

    private String companyToken;

    private ZonedDateTime companyTokenExpiration;

    public Internship(Student student) {
        this.student = student;
    }

    public Internship(Student student, Professor professor, Company company, String description) {
        this.student = student;
        this.coordinator = professor;
        this.company = company;
        this.status = InternshipStatus.SUBMITTED;
        this.journal = new ArrayList<>();
        this.description = description;
    }

    public Internship(Student student, Professor professor, Company company, String description,
                      LocalDate startDate, LocalDate endDate, boolean foreignCompany, int weeklyHours) {
        this.student = student;
        this.coordinator = professor;
        this.company = company;
        this.status = InternshipStatus.SUBMITTED;
        this.journal = new ArrayList<>();
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.foreignCompany = foreignCompany;
        this.weeklyHours = weeklyHours;
    }

    public String getFormattedDate(LocalDate date){
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }
}
