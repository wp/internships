package mk.ukim.finki.wp.internships.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.exception.EntityNotFoundException;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.User;
import mk.ukim.finki.wp.internships.repository.ProfessorRepository;
import mk.ukim.finki.wp.internships.repository.UserRepository;
import mk.ukim.finki.wp.internships.service.ProfessorService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProfessorServiceImpl implements ProfessorService {
    private final UserRepository userRepository;
    private final ProfessorRepository professorRepository;
    @Override
    public Professor getProfessorByUserId(String userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(userId));
        return professorRepository.findByEmail(user.getEmail());
    }

    @Override
    public List<Professor> listAll() {
        return professorRepository.findAll(Sort.by(Sort.Order.asc("name")));
    }
}

