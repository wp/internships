package mk.ukim.finki.wp.internships.repository;

import mk.ukim.finki.wp.internships.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company, String> {
    Optional<Company> findByName(String name);
}
