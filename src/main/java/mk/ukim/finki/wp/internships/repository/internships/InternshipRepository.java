package mk.ukim.finki.wp.internships.repository.internships;

import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.repository.JpaSpecificationRepository;

import java.util.List;
import java.util.Optional;

public interface InternshipRepository extends JpaSpecificationRepository<Internship, Long> {
    Internship findByCompanyToken(String companyToken);

}
