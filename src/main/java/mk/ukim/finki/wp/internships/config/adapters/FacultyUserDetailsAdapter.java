package mk.ukim.finki.wp.internships.config.adapters;

import mk.ukim.finki.wp.internships.config.FacultyUserDetails;
import mk.ukim.finki.wp.internships.model.User;

public interface FacultyUserDetailsAdapter {

    boolean supports(User user);

    FacultyUserDetails adapt(User user, String username);
}
