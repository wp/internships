package mk.ukim.finki.wp.internships.config;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.config.adapters.FacultyUserDetailsAdapter;
import mk.ukim.finki.wp.internships.exception.InvalidUsernameException;
import mk.ukim.finki.wp.internships.model.User;
import mk.ukim.finki.wp.internships.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class FacultyUserDetailsService implements UserDetailsService {

    final UserRepository userRepository;
    final List<FacultyUserDetailsAdapter> adapters;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findById(username).orElse(null);
        FacultyUserDetails userDetails = null;
        for (FacultyUserDetailsAdapter adapter : adapters) {
            if (adapter.supports(user)) {
                userDetails = adapter.adapt(user, username);
                if (userDetails != null) {
                    break;
                }
            }
        }
        if (userDetails == null) {
            throw new InvalidUsernameException("Failed to login user with username " + username);
        }
        return userDetails;
    }
}
