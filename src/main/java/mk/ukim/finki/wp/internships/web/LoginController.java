package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/login")
@Controller
@AllArgsConstructor
public class LoginController {
    @GetMapping
    public String getPage() {
        return "index";
    }
}
