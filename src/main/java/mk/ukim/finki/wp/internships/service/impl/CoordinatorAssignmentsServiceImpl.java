package mk.ukim.finki.wp.internships.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.model.internships.InternshipCoordinatorAssignmentsView;
import mk.ukim.finki.wp.internships.repository.CoordinatorAssignmentsRepository;
import mk.ukim.finki.wp.internships.service.CoordinatorAssignmentsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoordinatorAssignmentsServiceImpl implements CoordinatorAssignmentsService {
    private final CoordinatorAssignmentsRepository coordinatorAssignmentsRepository;
    private final List<String> coordinators;

    CoordinatorAssignmentsServiceImpl(CoordinatorAssignmentsRepository coordinatorAssignmentsRepository,
                                      @Value("${internships.coordinators}") String coordinatorList){
        this.coordinatorAssignmentsRepository = coordinatorAssignmentsRepository;
        this.coordinators = Arrays.asList(coordinatorList.split(","));
    }
    @Override
    public List<InternshipCoordinatorAssignmentsView> getCoordinatorAssignments() {
        return coordinatorAssignmentsRepository.findAll()
                .stream()
                .filter(assignment -> coordinators.contains(assignment.getProfessorId()))
                .collect(Collectors.toList());
    }
}
