package mk.ukim.finki.wp.internships.model.internships;

public enum InternshipStatus {
    SUBMITTED,
    ACCEPTED,
    REJECTED,
    JOURNAL_SUBMITTED,
    VALIDATED_BY_COMPANY,
    VALIDATED_BY_COORDINATOR,
    ARCHIVED
}
