package mk.ukim.finki.wp.internships.config.adapters;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.internships.config.FacultyUserDetails;
import mk.ukim.finki.wp.internships.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Order(10)
@RequiredArgsConstructor
@Component
public class BasicFacultyUserDetailsAdapter implements FacultyUserDetailsAdapter {

    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    private final PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(User user) {
        return user != null && !user.getRole().isProfessor();
    }

    @Override
    public FacultyUserDetails adapt(User user, String username) {
        return new FacultyUserDetails(user, passwordEncoder.encode(systemAuthenticationPassword));
    }
}
