package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/instructions")
@Controller
@AllArgsConstructor
public class InstructionsController {
    @GetMapping
    public String getPage() {
        return "index";
    }
}
