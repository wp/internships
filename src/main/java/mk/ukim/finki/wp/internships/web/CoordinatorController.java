package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.service.InternshipService;
import mk.ukim.finki.wp.internships.service.InternshipWeekService;
import mk.ukim.finki.wp.internships.service.ProfessorService;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("/coordinator-internships")
public class CoordinatorController {
    private final InternshipService internshipService;
    private final ProfessorService professorService;
    private final InternshipWeekService internshipWeekService;

    @GetMapping
    public String showList(@RequestParam (required = false) InternshipStatus status,
                           @RequestParam (required = false) String studentIndex,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "10") Integer results,
                           Model model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String professorUsername = authentication.getName();
        Professor professor = professorService.getProfessorByUserId(professorUsername);

        Page<Internship> internshipsPage = internshipService.listInternships(pageNum, results, status, studentIndex, null, professorUsername);

        model.addAttribute("statusSelected", status);
        model.addAttribute("studentIndexSelected", studentIndex);
        model.addAttribute("statuses", InternshipStatus.values());
        model.addAttribute("professor", professor);
        model.addAttribute("page", internshipsPage);

        return "coordinator/listInternships";
    }

    @GetMapping("/{internshipId}")
    public String showDetails (@PathVariable Long internshipId,
                               Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String professorUsername = authentication.getName();
        Professor professor = this.professorService.getProfessorByUserId(professorUsername);

        Internship internship = this.internshipService.findById(internshipId);
        Student student = internship.getStudent();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        boolean displayJournal = internship.getStatus() == InternshipStatus.VALIDATED_BY_COMPANY ||
                internship.getStatus() == InternshipStatus.VALIDATED_BY_COORDINATOR ||
                internship.getStatus() == InternshipStatus.ARCHIVED;
        int totalWeeks = journal.size();
        model.addAttribute("totalWeeks", totalWeeks);

        model.addAttribute("displayJournal", displayJournal);
        model.addAttribute("professor", professor);
        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("journal", journal);

        return "coordinator/journalDetails";
    }

    @PostMapping("/{internshipId}/internship-weeks/{weekId}/addWeekComment")
    public String addWeekComment(@PathVariable Long internshipId,
                                 @PathVariable Long weekId,
                                 @RequestParam String coordinatorComment) {
        if (coordinatorComment.isEmpty()){
            coordinatorComment = null;
        }
        this.internshipWeekService.updateCoordinatorComment(weekId, coordinatorComment);
        return "redirect:/coordinator-internships/" + internshipId;
    }

    @PostMapping("/{internshipId}/approve")
    public String approveInternship (@PathVariable Long internshipId) {
        this.internshipService.updateStatus(internshipId, InternshipStatus.VALIDATED_BY_COORDINATOR);
        return "redirect:/coordinator-internships/" + internshipId;
    }

    @PostMapping("/{internshipId}/disapprove")
    public String disapproveInternship (@PathVariable Long internshipId) {
        this.internshipService.updateStatus(internshipId, InternshipStatus.ACCEPTED);
        return "redirect:/coordinator-internships/" + internshipId;
    }

}
