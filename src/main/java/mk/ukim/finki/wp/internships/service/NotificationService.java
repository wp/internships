package mk.ukim.finki.wp.internships.service;

import mk.ukim.finki.wp.internships.model.dtos.MailSendingStatus;
import mk.ukim.finki.wp.internships.model.internships.Internship;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface NotificationService {

    List<CompletableFuture<MailSendingStatus>> notifyCompanyInternshipCreated(Internship internship);

    List<CompletableFuture<MailSendingStatus>> notifyCompanyInternshipJournalSubmitted(Internship internship);

}
