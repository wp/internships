package mk.ukim.finki.wp.internships.config.adapters;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.internships.config.FacultyUserDetails;
import mk.ukim.finki.wp.internships.model.Company;
import mk.ukim.finki.wp.internships.model.User;
import mk.ukim.finki.wp.internships.model.UserRole;
import mk.ukim.finki.wp.internships.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Order(100)
@RequiredArgsConstructor
@Component
public class CompanyFacultyUserDetailsAdapter implements FacultyUserDetailsAdapter {

    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    private final CompanyRepository companyRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(User user) {
        return user == null;
    }

    @Override
    public FacultyUserDetails adapt(User user, String username) {
        Company company = companyRepository.findById(username).orElse(null);
        if (company == null) {
            return null;
        }
        User user1 = new User();
        user1.setId(username);
        user1.setRole(UserRole.COMPANY);
        return new FacultyUserDetails(user1, company, passwordEncoder.encode(systemAuthenticationPassword));
    }
}
