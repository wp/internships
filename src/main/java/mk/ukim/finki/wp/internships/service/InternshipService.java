package mk.ukim.finki.wp.internships.service;

import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.dtos.MailSendingStatus;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface InternshipService {

    Internship delete(Long id);

    Internship findById(Long id);

    Internship findByCompanyToken(String companyToken);

    Internship updateStatus(Long id, InternshipStatus status);
    Internship updateStatus(Internship internship, InternshipStatus status);

    Page<Internship> listInternships(int page, int size, InternshipStatus status, String student, String companyId,
                                     String professor);

    void assignCoordinator(Long internshipId, String professorId);

    void createInternship(String studentIndex, String companyId, String description,
                          LocalDate startDate, LocalDate endDate, boolean foreignCompany, int weeklyHours);

    List<CompletableFuture<MailSendingStatus>> createInternshipEmail(String studentIndex, String companyId, String description,
                                                                LocalDate startDate, LocalDate endDate, boolean foreignCompany, int weeklyHours);

    void updateDates(Long internshipId, LocalDate startDate, LocalDate endDate);

    int getInternshipsCount();

    boolean isCompanyTokenExpired(Internship internship);

    String generateCompanyToken(Internship internship);

    List<CompletableFuture<MailSendingStatus>> submitJournal(Long internshipId);

    List<CompletableFuture<MailSendingStatus>> resendInternshipCreatedMail(Internship internship);

    List<CompletableFuture<MailSendingStatus>> resendJournalSubmittedMail(Internship internship);
}
