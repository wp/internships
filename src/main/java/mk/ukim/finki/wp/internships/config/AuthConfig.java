package mk.ukim.finki.wp.internships.config;

import mk.ukim.finki.wp.internships.model.enums.AppRole;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;

public class AuthConfig {

    public HttpSecurity authorize(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests((requests) -> requests
                        .requestMatchers(HttpMethod.OPTIONS).permitAll()
                        .requestMatchers("/student-internships/**").hasRole(AppRole.STUDENT.name())
                        .requestMatchers("/all-internships/**").hasRole(AppRole.ADMIN.name())
                        .requestMatchers("/coordinators/**").hasRole(AppRole.ADMIN.name())
                        .requestMatchers("/coordinator-internships/**").hasAnyRole(AppRole.PROFESSOR.name(), AppRole.ADMIN.name())
                        .requestMatchers("/company-internships/**").hasAnyRole(AppRole.COMPANY.name())
                        .requestMatchers("/company-action/**").permitAll()
                        .requestMatchers("io.png", "/", "", "/css/*", "/js/*", "/images/*", "/instructions").permitAll()
                        .anyRequest().authenticated()
                )
                .logout(LogoutConfigurer::permitAll);

    }

}
