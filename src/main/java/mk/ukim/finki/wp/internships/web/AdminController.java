package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.model.Company;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipCoordinatorAssignmentsView;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.service.CompanyService;
import mk.ukim.finki.wp.internships.service.CoordinatorAssignmentsService;
import mk.ukim.finki.wp.internships.service.InternshipService;
import mk.ukim.finki.wp.internships.service.ProfessorService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;


@Controller
@AllArgsConstructor
public class AdminController {
    private final InternshipService internshipService;
    private final CompanyService companyService;
    private final ProfessorService professorService;
    private final CoordinatorAssignmentsService coordinatorAssignmentsService;

    @GetMapping("/all-internships")
    public String getAllInternships(Model model,
                                    @RequestParam(required = false) String companyId,
                                    @RequestParam(required = false) InternshipStatus status,
                                    @RequestParam(required = false) String student,
                                    @RequestParam(required = false) String professor,
                                    @RequestParam(defaultValue = "1") Integer pageNum,
                                    @RequestParam(defaultValue = "20") Integer results) {

        Page<Internship> internshipsPage = internshipService.listInternships(pageNum, results, status, student, companyId, professor);
        List<Company> companies = companyService.getAllCompanies();
        List<Professor> professors = professorService.listAll();

        model.addAttribute("professors", professors);
        model.addAttribute("page", internshipsPage);
        model.addAttribute("statusSelected", status);
        model.addAttribute("professorSelected", professor);
        model.addAttribute("studentSelected", student);
        model.addAttribute("companySelected", companyId);
        model.addAttribute("companies", companies);
        model.addAttribute("statuses", InternshipStatus.values());
        return "admin/listInternships";
    }

    @GetMapping("/all-internships/{internshipId}")
    public String showDetails(@PathVariable Long internshipId,
                              Model model) {
        Internship internship = this.internshipService.findById(internshipId);
        Student student = internship.getStudent();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        int totalWeeks = journal.size();
        model.addAttribute("totalWeeks", totalWeeks);

        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("journal", journal);

        return "admin/details";
    }

    @PostMapping("/all-internships/{internshipId}/assign-coordinator")
    public String assignCoordinator(@PathVariable Long internshipId,
                                    @RequestParam String professorId) {

        this.internshipService.assignCoordinator(internshipId, professorId);
        return "redirect:/all-internships";
    }

    @GetMapping("/coordinators")
    public String getCoordinators(Model model) {
        List<InternshipCoordinatorAssignmentsView> coordinatorAssignments = coordinatorAssignmentsService.getCoordinatorAssignments();
        int totalInternships = internshipService.getInternshipsCount();

        model.addAttribute("totalInternships", totalInternships);
        model.addAttribute("coordinatorAssignments", coordinatorAssignments);
        return "admin/coordinators";
    }

    @PostMapping("/all-internships/{internshipId}/archive")
    public String archiveInternship(@PathVariable Long internshipId) {
        internshipService.updateStatus(internshipId, InternshipStatus.ARCHIVED);
        return "redirect:/all-internships";
    }

}
