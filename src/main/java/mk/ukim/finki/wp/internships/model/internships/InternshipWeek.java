package mk.ukim.finki.wp.internships.model.internships;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class InternshipWeek {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate startDate;

    private LocalDate endDate;

    @ManyToOne
    @JoinColumn(name="internship_id")
    private Internship internship;

    private String description;

    private String companyComment;

    private String coordinatorComment;

    private Integer workingHours;

    public InternshipWeek(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public InternshipWeek(LocalDate startDate, LocalDate endDate, Internship internship, String description,
                          int workingHours) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.internship = internship;
        this.description = description;
        this.workingHours = workingHours;
    }

    public String getFormattedDate(LocalDate date){
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }
}
