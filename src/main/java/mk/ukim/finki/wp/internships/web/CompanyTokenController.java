package mk.ukim.finki.wp.internships.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.model.internships.InternshipWeek;
import mk.ukim.finki.wp.internships.service.InternshipService;
import mk.ukim.finki.wp.internships.service.InternshipWeekService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("/company-action")
public class CompanyTokenController {
    private final InternshipService internshipService;
    private final InternshipWeekService internshipWeekService;

    @GetMapping("/accept/{companyToken}")
    public String acceptInternship(@PathVariable String companyToken,
                                   Model model) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (internship.getStatus() != InternshipStatus.SUBMITTED) {
            return handleError(model, internship.getStatus(),
                    "Invalid action! Internship can be accepted/rejected only when the status is SUBMITTED.",
                    "Невалидна акција! Праксата може да биде прифатена/одбиена само кога статусот е SUBMITTED.");
        } else if (!internshipService.isCompanyTokenExpired(internship)) {
            internshipService.updateStatus(internship, InternshipStatus.ACCEPTED);
            return handleSuccess(model, "Internship has been accepted successfully!",
                    "Барањето за пракса е успешно прифатено!");
        } else if (internshipService.isCompanyTokenExpired(internship)) {
            return handleTokenExpired(model, internship, true);
        }
        return "error";
    }

    @GetMapping("/reject/{companyToken}")
    public String rejectInternship(@PathVariable String companyToken,
                                   Model model) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (internship.getStatus() != InternshipStatus.SUBMITTED) {
            return handleError(model, internship.getStatus(),
                    "Invalid action! Internship can be accepted/rejected only when the status is SUBMITTED.",
                    "Невалидна акција! Праксата може да биде прифатена/одбиена само кога статусот е SUBMITTED.");
        } else if (!internshipService.isCompanyTokenExpired(internship)) {
            internshipService.updateStatus(internship, InternshipStatus.REJECTED);
            return handleSuccess(model, "Internship has been rejected.",
                    "Барањето за пракса е успешно одбиено.");
        } else if (internshipService.isCompanyTokenExpired(internship)) {
            return handleTokenExpired(model, internship, true);
        }
        return "error";
    }

    @GetMapping("/valid/{companyToken}")
    public String validateInternship(@PathVariable String companyToken,
                                     Model model) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (internship.getStatus() != InternshipStatus.JOURNAL_SUBMITTED) {
            return handleError(model, internship.getStatus(),
                    "Invalid action! Internship can be validated/invalidated only when the status is JOURNAL_SUBMITTED.",
                    "Невалидна акција! Праксата може да биде валидирана/одбиена само кога статусот е JOURNAL_SUBMITTED.");
        } else if (!internshipService.isCompanyTokenExpired(internship)) {
            internshipService.updateStatus(internship, InternshipStatus.VALIDATED_BY_COMPANY);
            return handleSuccess(model, "Internship journal has been successfully validated!",
                    "Праксата е успешно валидирана!");
        } else if (internshipService.isCompanyTokenExpired(internship)) {
            return handleTokenExpired(model, internship, false);
        }
        return "error";
    }

    @GetMapping("/invalid/{companyToken}")
    public String internshipNotValid(@PathVariable String companyToken,
                                     Model model) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (internship.getStatus() != InternshipStatus.JOURNAL_SUBMITTED) {
            return handleError(model, internship.getStatus(),
                    "Invalid action! Internship can be validated/invalidated only when the status is JOURNAL_SUBMITTED.",
                    "Невалидна акција! Праксата може да биде валидирана/одбиена само кога статусот е JOURNAL_SUBMITTED.");
        } else if (!internshipService.isCompanyTokenExpired(internship)) {
            internshipService.updateStatus(internship, InternshipStatus.ACCEPTED);
            return handleSuccess(model, "Internship journal is successfully marked as invalid.",
                    "Праксата е успешно обележана како невалидна.");
        } else if (internshipService.isCompanyTokenExpired(internship)) {
            return handleTokenExpired(model, internship, false);
        }
        return "error";
    }

    @GetMapping("/journal/{companyToken}")
    public String viewInternshipDetails(@PathVariable String companyToken,
                                        Model model) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (internshipService.isCompanyTokenExpired(internship)) {
            internshipService.resendJournalSubmittedMail(internship);
            return handleTokenExpired(model, internship, false);
        }
        Student student = internship.getStudent();
        List<InternshipWeek> journal = internship.getJournal();
        journal.sort(Comparator.comparing(InternshipWeek::getStartDate));
        boolean displayJournal = internship.getStatus() == InternshipStatus.JOURNAL_SUBMITTED ||
                internship.getStatus() == InternshipStatus.VALIDATED_BY_COMPANY ||
                internship.getStatus() == InternshipStatus.VALIDATED_BY_COORDINATOR ||
                internship.getStatus() == InternshipStatus.ARCHIVED;
        int totalWeeks = journal.size();
        model.addAttribute("totalWeeks", totalWeeks);

        model.addAttribute("internship", internship);
        model.addAttribute("student", student);
        model.addAttribute("displayJournal", displayJournal);
        model.addAttribute("journal", journal);
        model.addAttribute("companyToken", companyToken);

        return "company/journalDetailsToken";
    }

    @PostMapping("/{companyToken}/internship-weeks/{weekId}/addWeekComment")
    public String addWeekComment(@PathVariable String companyToken,
                                 @PathVariable Long weekId,
                                 @RequestParam String companyComment) {
        Internship internship = internshipService.findByCompanyToken(companyToken);
        if (internship == null) return "error";
        if (companyComment.isEmpty()) {
            companyComment = null;
        }
        this.internshipWeekService.updateCompanyComment(weekId, companyComment);
        return "redirect:/company-action/journal/" + companyToken;
    }

    private String handleTokenExpired(Model model, Internship internship, boolean createdMail) {
        model.addAttribute("message", "The token has expired. A new email will be sent shortly.");
        model.addAttribute("messageMk", "Токенот е истечен. Наскоро ќе добиете нов емаил.");
        if (createdMail)
            internshipService.resendInternshipCreatedMail(internship);
        else
            internshipService.resendJournalSubmittedMail(internship);
        return "company/internshipStatusMessage";
    }

    private String handleError(Model model, InternshipStatus status, String customMessageEn, String customMessageMk) {
        model.addAttribute("errorMessage", customMessageEn + " Current status: " + status + ".");
        model.addAttribute("errorMessageMk", customMessageMk + " Моментален статус: " + status + ".");
        return "company/internshipStatusMessage";
    }

    private String handleSuccess(Model model, String messageEn, String messageMk) {
        model.addAttribute("message", messageEn);
        model.addAttribute("messageMk", messageMk);
        return "company/internshipStatusMessage";
    }

}
