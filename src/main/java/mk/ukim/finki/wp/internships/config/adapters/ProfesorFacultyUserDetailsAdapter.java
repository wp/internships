package mk.ukim.finki.wp.internships.config.adapters;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.internships.config.FacultyUserDetails;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.User;
import mk.ukim.finki.wp.internships.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Order(50)
@RequiredArgsConstructor
@Component
public class ProfesorFacultyUserDetailsAdapter implements FacultyUserDetailsAdapter {

    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    private final ProfessorRepository professorRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(User user) {
        return user != null && user.getRole().isProfessor();
    }

    @Override
    public FacultyUserDetails adapt(User user, String username) {
        Professor professor = professorRepository.findById(username.toLowerCase()).orElse(null);
        if (professor == null) {
            return null;
        }
        return new FacultyUserDetails(user, professor, passwordEncoder.encode(systemAuthenticationPassword));
    }
}
