package mk.ukim.finki.wp.internships.service;

import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;

import java.util.List;

public interface ProfessorService {
    Professor getProfessorByUserId(String userId);
    List<Professor> listAll();
}
