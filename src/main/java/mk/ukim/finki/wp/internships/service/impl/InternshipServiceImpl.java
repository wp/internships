package mk.ukim.finki.wp.internships.service.impl;

import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.internships.exception.EntityNotFoundException;
import mk.ukim.finki.wp.internships.exception.InternshipNotFoundException;
import mk.ukim.finki.wp.internships.exception.ProfessorNotFoundException;
import mk.ukim.finki.wp.internships.model.Company;
import mk.ukim.finki.wp.internships.model.Professor;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.dtos.MailSendingStatus;
import mk.ukim.finki.wp.internships.model.internships.Internship;
import mk.ukim.finki.wp.internships.model.internships.InternshipStatus;
import mk.ukim.finki.wp.internships.repository.CompanyRepository;
import mk.ukim.finki.wp.internships.repository.ProfessorRepository;
import mk.ukim.finki.wp.internships.repository.StudentRepository;
import mk.ukim.finki.wp.internships.repository.internships.InternshipRepository;
import mk.ukim.finki.wp.internships.service.InternshipService;
import mk.ukim.finki.wp.internships.service.InternshipWeekService;
import mk.ukim.finki.wp.internships.service.NotificationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static mk.ukim.finki.wp.internships.specifications.FieldFilterSpecification.*;

@Service
public class InternshipServiceImpl implements InternshipService {
    private final InternshipRepository internshipRepository;
    private final StudentRepository studentRepository;
    private final InternshipWeekService internshipWeekService;
    private final CompanyRepository companyRepository;
    private final ProfessorRepository professorRepository;
    private final CoordinatorAssignmentService coordinatorAssignmentService;
    private final NotificationService notificationService;
    @Value("${internships.company.token.expiration-days}")
    private int tokenExpirationDays;

    public InternshipServiceImpl(InternshipRepository internshipRepository, StudentRepository studentRepository,
                                 InternshipWeekService internshipWeekService, CompanyRepository companyRepository,
                                 ProfessorRepository professorRepository,
                                 CoordinatorAssignmentService coordinatorAssignmentService,
                                 NotificationService notificationService,
                                 @Value("${internships.company.token.expiration-days}") int tokenExpirationDays) {
        this.internshipRepository = internshipRepository;
        this.studentRepository = studentRepository;
        this.internshipWeekService = internshipWeekService;
        this.companyRepository = companyRepository;
        this.professorRepository = professorRepository;
        this.coordinatorAssignmentService = coordinatorAssignmentService;
        this.notificationService = notificationService;
        this.tokenExpirationDays = tokenExpirationDays;
    }

    @Override
    public Internship delete(Long id) {
        Internship internship = findById(id);
        internshipRepository.delete(internship);
        return internship;
    }

    @Override
    public Internship findById(Long id) {
        return internshipRepository.findById(id).orElseThrow(() -> new InternshipNotFoundException(id));
    }

    @Override
    public Internship findByCompanyToken(String companyToken) {
        return internshipRepository.findByCompanyToken(companyToken);
    }

    @Override
    public Internship updateStatus(Long id, InternshipStatus status) {
        Internship internship = internshipRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Internship with id " + id + " not found"));
        internship.setStatus(status);
        return internshipRepository.save(internship);
    }
    @Override
    public Internship updateStatus(Internship internship, InternshipStatus status) {
        internship.setStatus(status);
        return internshipRepository.save(internship);
    }


    @Override
    public Page<Internship> listInternships(int page, int size, InternshipStatus status, String student, String companyId,
                                            String professor) {
        Specification<Internship> spec = Specification.where(null);

        if (status != null) {
            spec = spec.and(filterEqualsV(Internship.class, "status", status));
        }

        if (companyId != null && !companyId.trim().isEmpty()) {
            spec = spec.and(filterEqualsV(Internship.class, "company.id", companyId));
        }

        if (professor != null && !professor.trim().isEmpty()) {
            Specification<Internship> professorSpec = filterContainsText(Internship.class, "coordinator.id", professor)
                    .or(filterContainsText(Internship.class, "coordinator.name", professor));
            spec = spec.and(professorSpec);
        }

        if (student != null && !student.trim().isEmpty()) {
            Specification<Internship> studentSpec = filterContainsText(Internship.class, "student.index", student)
                    .or(filterContainsText(Internship.class, "student.name", student))
                    .or(filterContainsText(Internship.class, "student.lastName", student));
            spec = spec.and(studentSpec);
        }

        return internshipRepository.findAll(spec, PageRequest.of(page - 1, size, Sort.by(Sort.Direction.DESC, "startDate")));
    }

    @Override
    public void assignCoordinator(Long internshipId, String professorId) {
        Internship internship = internshipRepository.findById(internshipId)
                .orElseThrow(() -> new EntityNotFoundException("Internship with id " + internshipId + " not found"));
        Professor coordinator = professorRepository.findById(professorId)
                .orElseThrow(() -> new EntityNotFoundException("Professor with id " + professorId + " not found"));
        internship.setCoordinator(coordinator);
        internshipRepository.save(internship);
    }

    @Override
    public void createInternship(String studentIndex, String companyId, String description, LocalDate startDate,
                                 LocalDate endDate, boolean foreignCompany, int weeklyHours) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid company ID."));

        Student student = studentRepository.findByIndex(studentIndex);
        if (student == null) {
            throw new IllegalArgumentException("Invalid student index.");
        }
        String coordinatorId = coordinatorAssignmentService.getNextCoordinator();
        Professor coordinator = professorRepository.findById(coordinatorId).orElseThrow(() -> new ProfessorNotFoundException(coordinatorId));
        Internship internship = new Internship(student, coordinator, company, description, startDate, endDate,
                foreignCompany, weeklyHours);
        Internship savedInternship = internshipRepository.save(internship);

        LocalDate currentWeekStart = startDate;
        while (!currentWeekStart.isAfter(endDate) && !currentWeekStart.isEqual(endDate)) {
            LocalDate currentWeekEnd = currentWeekStart.plusWeeks(1);
            if (currentWeekEnd.isAfter(endDate)) {
                currentWeekEnd = endDate;
            }

            this.internshipWeekService.create(currentWeekStart, currentWeekEnd, savedInternship.getId(), "", weeklyHours);

            currentWeekStart = currentWeekEnd;
        }
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> createInternshipEmail(String studentIndex, String companyId, String description, LocalDate startDate, LocalDate endDate, boolean foreignCompany, int weeklyHours) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid company ID."));

        Student student = studentRepository.findByIndex(studentIndex);
        if (student == null) {
            throw new IllegalArgumentException("Invalid student index.");
        }
        String coordinatorId = coordinatorAssignmentService.getNextCoordinator();
        Professor coordinator = professorRepository.findById(coordinatorId).orElseThrow(() -> new ProfessorNotFoundException(coordinatorId));
        Internship internship = new Internship(student, coordinator, company, description, startDate, endDate,
                foreignCompany, weeklyHours);
        String companyToken = UUID.randomUUID().toString();
        internship.setCompanyToken(companyToken);
        internship.setCompanyTokenExpiration(ZonedDateTime.now().plusDays(tokenExpirationDays));
        Internship savedInternship = internshipRepository.save(internship);

        LocalDate currentWeekStart = startDate;
        while (!currentWeekStart.isAfter(endDate) && !currentWeekStart.isEqual(endDate)) {
            LocalDate currentWeekEnd = currentWeekStart.plusWeeks(1);
            if (currentWeekEnd.isAfter(endDate)) {
                currentWeekEnd = endDate;
            }

            this.internshipWeekService.create(currentWeekStart, currentWeekEnd, savedInternship.getId(), "", weeklyHours);

            currentWeekStart = currentWeekEnd;
        }
        return notificationService.notifyCompanyInternshipCreated(savedInternship);
    }

    @Override
    public void updateDates(Long internshipId, LocalDate startDate, LocalDate endDate) {
        Internship internship = internshipRepository.findById(internshipId)
                .orElseThrow(() -> new EntityNotFoundException("Internship with id " + internshipId + " not found"));
        internship.setStartDate(startDate);
        internship.setEndDate(endDate);
        internshipRepository.save(internship);
    }

    @Override
    public int getInternshipsCount() {
        return (int) internshipRepository.count();
    }

    @Override
    public boolean isCompanyTokenExpired(Internship internship) {
        return internship.getCompanyTokenExpiration().isBefore(ZonedDateTime.now());
    }

    @Override
    public String generateCompanyToken(Internship internship) {
        String newToken = UUID.randomUUID().toString();
        internship.setCompanyToken(newToken);
        internship.setCompanyTokenExpiration(ZonedDateTime.now().plusDays(tokenExpirationDays));
        internshipRepository.save(internship);
        return newToken;
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> submitJournal(Long internshipId) {
        Internship internship = internshipRepository.findById(internshipId)
                .orElseThrow(() -> new EntityNotFoundException("Internship with id " + internshipId + " not found"));
        internship.setStatus(InternshipStatus.JOURNAL_SUBMITTED);
        internshipRepository.save(internship);
        if (internship.getCompanyToken() == null)
            generateCompanyToken(internship);
        return notificationService.notifyCompanyInternshipJournalSubmitted(internship);
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> resendInternshipCreatedMail(Internship internship) {
        generateCompanyToken(internship);
        return notificationService.notifyCompanyInternshipCreated(internship);
    }

    @Override
    public List<CompletableFuture<MailSendingStatus>> resendJournalSubmittedMail(Internship internship) {
        generateCompanyToken(internship);
        return notificationService.notifyCompanyInternshipJournalSubmitted(internship);
    }


}
