package mk.ukim.finki.wp.internships.repository;

import mk.ukim.finki.wp.internships.model.internships.InternshipCoordinatorAssignmentsView;

public interface CoordinatorAssignmentsRepository extends JpaSpecificationRepository<InternshipCoordinatorAssignmentsView, String> {
}
