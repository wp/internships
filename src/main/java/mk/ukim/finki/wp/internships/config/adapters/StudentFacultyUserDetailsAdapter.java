package mk.ukim.finki.wp.internships.config.adapters;

import lombok.RequiredArgsConstructor;
import mk.ukim.finki.wp.internships.config.FacultyUserDetails;
import mk.ukim.finki.wp.internships.model.Student;
import mk.ukim.finki.wp.internships.model.User;
import mk.ukim.finki.wp.internships.model.UserRole;
import mk.ukim.finki.wp.internships.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Order(90)
@RequiredArgsConstructor
@Component
public class StudentFacultyUserDetailsAdapter implements FacultyUserDetailsAdapter {

    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    private final StudentRepository studentRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public boolean supports(User user) {
        return user == null;
    }

    @Override
    public FacultyUserDetails adapt(User user, String username) {
        Student student = studentRepository.findById(username.toLowerCase()).orElse(null);
        if (student == null) {
            return null;
        }
        User user1 = new User();
        user1.setId(username);
        user1.setRole(UserRole.STUDENT);
        return new FacultyUserDetails(user1, student, passwordEncoder.encode(systemAuthenticationPassword));
    }
}
